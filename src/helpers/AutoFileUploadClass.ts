import { AxiosResponse } from "axios";
import { humanFileSize } from "./FileSizeHelper";

export type FileUploadStatus = "new" | "in_progress" | "completed" | "failed" | "tooBig";
export type FileUploadEventType = "started" | "completed" | "error" | "cancel" | "remove" | "tooBig";

type FileUploadEventListener = (fileUpload: FileUpload) => void;

let uidIncrementor = 1;

export class FileUpload {
  uid: number;
  private _file: File | Blob;
  private _progress = 0;
  private _error = undefined as Error | undefined;
  private _response = undefined as AxiosResponse<unknown> | Response | undefined;
  private _tooBigFlag = false;
  private _newFlag = true;

  private _listeners = {} as Record<
    FileUploadEventType,
    FileUploadEventListener[]
  >;

  constructor(file: File | Blob) {
  	this.uid = uidIncrementor++;
  	this._file = file;
  }

  addEventListener(
  	type: FileUploadEventType,
  	listener: FileUploadEventListener
  ): () => void {
  	if (!(type in this._listeners)) {
  		this._listeners[type] = [];
  	}
  	this._listeners[type]?.push(listener);

  	return () => {
  		this.removeEventListener(type, listener);
  	};
  }

  removeEventListener(
  	type: FileUploadEventType,
  	listener: FileUploadEventListener
  ): void {
  	if (!(type in this._listeners)) {
  		return;
  	}
  	const stack = this._listeners[type];
  	for (let i = 0, l = stack.length; i < l; i++) {
  		if (stack[i] === listener) {
  			stack.splice(i, 1);
  			return;
  		}
  	}
  }

  private dispatchEvent(type: FileUploadEventType): void {
  	if (!(type in this._listeners)) {
  		return;
  	}
  	const stack = this._listeners[type].slice();
  	stack.forEach(listener => listener(this));
  }

  reportStart() {
  	if (this._newFlag) {
		  this.dispatchEvent("started");
		  console.log(`Started upload on FileUpload (id=${this.uid})`);
  	} else {
  		  console.warn(`Started upload on FileUpload (id=${this.uid}) with status ${this.status}`);
  	}

	  this._newFlag = false;
  }

  setProgress(progress: number) {
  	if (typeof progress !== "number" || progress < 0 || progress > 100) {
  		throw new Error(
  			"Could not set progress. Please enter a number between 0 and 100 (inclusive)"
  		);
  	}
  	this._progress = progress;
  }

  get progress() {
  	return this._progress;
  }

  reportResponse(response: AxiosResponse<unknown> | Response) {
  	this._response = response;
  	this.setProgress(100);
  	this.dispatchEvent("completed");
  }

  get response() {
  	return this._response;
  }

  reportError(e: Error) {
  	this.dispatchEvent("error");
  	this._error = e;
  }

  get error() {
  	return this._error;
  }

  get errorMessage() {
  	return this._error?.message;
  }

  cancelUpload() {
  	this.dispatchEvent("cancel");
  }

  removeFromQueue() {
  	this.cancelUpload();
  	this.dispatchEvent("remove");
  }

  get status(): FileUploadStatus {
  	if (this._tooBigFlag) {
  		return "tooBig";
  	} else if (this._error) {
  		return "failed";
  	} else if (this._response) {
  		return "completed";
  	} else if (this._newFlag) {
  		return "new";
  	} 

  	return "in_progress";
  }

  get file() {
  	return this._file;
  }

  get fileSize() {
  	return this._file.size;
  }

  checkFileSize(referenceSize?: number): void  {
  	if (this.isFileBiggerThan(referenceSize)) {
  		this.dispatchEvent("tooBig");
	 	this._tooBigFlag = true;
  	}
  }

  isFileBiggerThan(referenceSize?: number): boolean | undefined {
	  if(!referenceSize) {
		  return undefined;
	  }
	  return this.fileSize > referenceSize;
  }

  get fileSizeFormatted() {
  	return humanFileSize(this.fileSize);
  }

  get fileName() {
  	if (this._file instanceof File) {
  		return this._file.name;
  	} else {
  		return "File";
  	}
  }
}
