import { FileUpload } from "../AutoFileUploadClass";

export type FileUploadFunction = (fileUpload: FileUpload) => Promise<unknown>;

export function defaultPrepareRequest(file: File | Blob, fileKey: string): FormData {
	const formData = new FormData();
	formData.set(fileKey, file);
  
	return formData;
}
  