import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { FileUpload } from "../AutoFileUploadClass";
import { PrepareRequestMethod } from "../AutoFileUploadHelper";
import { defaultPrepareRequest, FileUploadFunction } from "./MiscHelpers";
import {RequestHelper} from "./RequestHelperFactory";


export class AxiosRequestHelper implements RequestHelper {
    axiosInstance: AxiosInstance;
    url: string;
    usePatch: boolean;
    fileKey: string;
    prepareRequest?: PrepareRequestMethod;

    constructor(axiosInstance: AxiosInstance, url: string, usePatch: boolean, fileKey: string, prepareRequest?: PrepareRequestMethod) {
    	this.url = url;
    	this.usePatch = usePatch;
    	this.fileKey = fileKey;
    	this.axiosInstance = axiosInstance;
    	this.prepareRequest = prepareRequest;
    }

    createUploadMethod(): FileUploadFunction {
    	return (fileUpload: FileUpload) => {
    		fileUpload.reportStart();

    		const formData = this.prepareFormData(fileUpload.file);
    		const axiosConfig = this.createAxiosConfig(fileUpload);

    		return this.axiosInstance
    			.post(this.url, formData, axiosConfig)
    			.then(response => {
    				fileUpload.reportResponse(response);
    			})
    			.catch(e => {
    				fileUpload.reportError(e);
    			});
    	};
    }

    private prepareFormData(file: File | Blob) {
    	const formData = this.prepareRequest
    		? this.prepareRequest(file)
    		: defaultPrepareRequest(file, this.fileKey);
            
    	if (this.usePatch) {
    		formData.set("_method", "patch");
    	}

    	return formData;
    }

    private createAxiosConfig(fileUpload: FileUpload): AxiosRequestConfig {
    	const source = axios.CancelToken.source();
      
    	const handleCancelUpload = () => {
    		source.cancel("Canceled by user");
    	};
      
    	fileUpload.addEventListener("cancel", handleCancelUpload);
      
    	return {
    		headers: {
    			"Content-Type": "application/json"
    		},
    		onUploadProgress: this.createOnUploadProgress(fileUpload),
    		cancelToken: source.token
    	};
    }

    private createOnUploadProgress (fileUpload: FileUpload): (progressEvent: ProgressEvent) => void {
    	return (progressEvent: ProgressEvent) => {
    		const percentCompleted = Math.round(
    			(progressEvent.loaded * 100) / progressEvent.total
    		);
    		fileUpload.setProgress(percentCompleted);
    	};
    }  
}
