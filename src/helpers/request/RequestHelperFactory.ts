import { AxiosInstance } from "axios";
import { PrepareRequestMethod } from "@/helpers/AutoFileUploadHelper";
import { AxiosRequestHelper } from "./AxiosRequestHelper";
import { FileUploadFunction } from "./MiscHelpers";

export interface RequestHelper {
    createUploadMethod(): FileUploadFunction
}

export type RequestLocation = {
    
};

export function requestHelperFactoryPatch(
	patchAction: string,
	fileKey: string,
	axiosInstance?: AxiosInstance,
	prepareRequest?: PrepareRequestMethod) {

	return requestHelperFactory(patchAction, true, fileKey, axiosInstance, prepareRequest);
}

export function requestHelperFactoryPost(
	postAction: string,
	fileKey: string,
	axiosInstance?: AxiosInstance,
	prepareRequest?: PrepareRequestMethod) {

	return requestHelperFactory(postAction, false, fileKey, axiosInstance, prepareRequest);
}


function requestHelperFactory(
	url: string,
	usePatch: boolean,
	fileKey: string,
	axiosInstance?: AxiosInstance,
	prepareRequest?: PrepareRequestMethod,
): RequestHelper {
	if (axiosInstance) {
		return new AxiosRequestHelper(axiosInstance, url, usePatch, fileKey, prepareRequest);
	} else {
		throw Error("axios alternative not yet implemented");
	}
}