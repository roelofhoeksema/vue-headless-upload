
export type PrepareRequestMethod = (file: File | Blob) => FormData;

export function fileListToArray(fileList?: FileList | null): File[] {
	if (!fileList) {
		return [];
	}
	return Array.from(fileList);
}
