#abandonware
Unfortunately this repo became abandonware the second it was uploaded. I don't have the time to properly fix/maintain this package in my current situation. Sorry!

# Vue headless upload component

Headless implementation of file upload component.


## Todo's
- Check if drag/drop is supported by browser
- Documentation
- Testing
- Refactor Axios to XMLHttpRequest
