## Simple server for testing the component. 

##### Start server:
`node server.js 8085`
Defaults to listening on port 8085

##### Routes
Accepts requests on
`/test/post` and `/test/patch`

The file should have key `file`.
Other data can be send in text form.

##### Response
It echos back the values send in `file` and `body` objects