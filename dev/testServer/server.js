var express = require("express");
var cors = require("cors");
const multer = require("multer");

const app = express();
app.use(cors());
app.use(express.json());



// SET STORAGE
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, "uploads");
	},
	filename: function (req, file, cb) {
		cb(null, file.fieldname + "-" + Date.now());
	}
});

const upload = multer({storage});

app.post("/test/post", upload.single("file"), function (req, res, next) {
	res.json({body: req.body, file: req.file});
});

app.patch("/test/patch", upload.single("file"), function (req, res, next) {
	res.json({requestBody: req.body});
});

// eslint-disable-next-line no-undef
const port = process.argv[2] ?? 8085;

const server = app.listen(port, "localhost", function () {
	const address = server.address();
	console.log(`Web server listening on ${address.address}:${address.port}`);
});